public class NodoInt {
	public int elemento;
	public NodoInt siguiente;

	public NodoInt(int elemento)
	{
		this.elemento = elemento;
		this.siguiente = null;
	}
}
