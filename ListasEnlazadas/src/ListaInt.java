
public class ListaInt {
	private NodoInt primero;
	
	public ListaInt()
	{
		this.primero = null;
	}
	
	public void imprimir()
	{
		System.out.print("[ ");
		NodoInt actual = this.primero;
		while(actual != null)
		{
			System.out.print(actual.elemento + " ");
			actual = actual.siguiente;
		}
		System.out.println("]");
	}
	
	public void agregarAdelante(int n)
	{
		NodoInt nuevo = new NodoInt(n);
		nuevo.siguiente = this.primero;
		this.primero = nuevo;
	}
	
	public int largo()
	{
		int contador = 0;
		NodoInt actual = this.primero;
		
		while(actual != null)
		{
			contador++;
			actual = actual.siguiente;
		}
		
		return contador;
	}
	
	public void agregarAtras(int n)
	{
		NodoInt nuevo = new NodoInt(n);
		
		if(this.primero == null)
			this.primero = nuevo;
		else
		{
			NodoInt actual = this.primero;
			
			while(actual.siguiente != null)
				actual = actual.siguiente;
			
			actual.siguiente = nuevo;	
		}
	}
	
	public void quitar(int n)
	{
		if(this.primero == null)
			return;
		
		if(this.primero.elemento == n)
			this.primero = this.primero.siguiente;
		else
		{
			NodoInt actual = this.primero;
			while(actual.siguiente != null && actual.siguiente.elemento != n)
				actual = actual.siguiente;
			
			//O se me termin� la lista o encontr� el elemento
			if(actual.siguiente != null)
			{
				//Si no se me termin� la lista, es porque encontr� el elemento a quitar.
				actual.siguiente = actual.siguiente.siguiente;
			}
		}
	}
	

}
