
public class Pruebas {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ListaInt lista = new ListaInt();
		
		lista.imprimir();
		System.out.println("Largo: " + lista.largo());
		lista.agregarAtras(5);
		
		lista.imprimir();
		System.out.println("Largo: " + lista.largo());
		lista.agregarAtras(3);
		
		lista.imprimir();
		System.out.println("Largo: " + lista.largo());
		lista.agregarAtras(1);
		
		lista.agregarAtras(1);
		
		lista.imprimir();
		
		System.out.println("Largo: " + lista.largo());
		
		lista.quitar(77);
		lista.imprimir();
		
		lista.quitar(1);
		lista.imprimir();
		lista.quitar(1);
		lista.imprimir();
		lista.quitar(3);
		lista.imprimir();
		lista.quitar(5);
		lista.imprimir();
		lista.quitar(5);
		lista.imprimir();
	}

}
