Ejercicios Listas Enlazadas
1. void quitarTodos(int e): que elimina todas las apariciones del elemento e en la lista. 
2. void recortarDesde(int k):  elimina los nodos desde la posición k hasta el final de la lista.
3. void recortarHasta(int k):   elimina los nodos desde la primera posicion hasta la posición k de la lista.
4. ListaInt sublista(int desde, int hasta): devuelve una nueva lista que contiene los nodos desde la posición desde hasta la posición hasta inclusive.
